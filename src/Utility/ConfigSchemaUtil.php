<?php

/**
 * @file
 * Contains \Drupal\config_schema\Utility\ConfigSchemaUtil.
 */

namespace Drupal\config_schema\Utility;

/**
 * Provides utilities for config schema elements.
 */
class ConfigSchemaUtil {
  /**
   * Gets all contained typed data properties as plain array.
   *
   * @param array|object $schema
   *   An array of config elements with key.
   *
   * @return array
   *   List of Element objects indexed by full name (keys with dot notation).
   */
  public static function convertConfigElementToList($schema) {
    $list = array();
    foreach ($schema as $key => $element) {
      if ($element instanceof Element) {
        foreach (self::convertConfigElementToList($element) as $sub_key => $element) {
          $list[$key . '.' . $sub_key] = $element;
        }
      }
      else {
        $list[$key] = $element;
      }
    }
    return $list;
  }
}
