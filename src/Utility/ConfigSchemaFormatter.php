<?php

/**
 * @file
 * Contains \Drupal\config_schema\Utility\ConfigSchemaFormatter.
 */

namespace Drupal\config_schema\Utility;

use Drupal\Component\Serialization\Yaml;
use Drupal\Component\Utility\String;

/**
 * Provides utilities for config schema elements.
 */
class ConfigSchemaFormatter {

  /**
   * Format config schema as list table.
   */
  protected static function formatSchemaAsList($schema) {
    $rows = array();
    $schema = ConfigSchemaUtil::convertConfigElementToList($schema);
    foreach ($schema as $key => $element) {
      $definition = $element->getDataDefinition();
      $rows[] = array(
        $key,
        $definition['label'],
        $definition['type'],
        $element->getString(),
      );
    }
    return array(
      '#type' => 'table',
      '#header' => array(
        t('Name'),
        t('Label'),
        t('Type'),
        t('Value'),
      ),
      '#rows' => $rows,
    );
  }

  /**
   * Format config schema as a tree.
   */
  public static function formatSchemaAsTree($schema, $collapsed = FALSE, $base_key = '') {
    $build = array();
    foreach ($schema as $key => $element) {
      // @todo Clean up array/DataDefinition
      //$definition = $element->getDataDefinition() + array('label' => t('N/A'));
      $definition = $element->getDataDefinition();
      $definition = is_object($definition) ? $definition->toArray() : $definition;
      $definition += array('label' => t('N/A'));

      $type = $definition['type'];
      $element_key = $base_key . $key;
      if ($element instanceof Element) {
        $build[$key] = array(
            '#type' => 'details',
            '#title' => $definition['label'],
            '#description' => $element_key . ' (' . $type . ')',
            '#collapsible' => TRUE,
            '#collapsed' => $collapsed,
          ) + static::formatSchemaASTree($element, TRUE, $element_key . '.');
      }
      else {
        $value = $element->getString();
        $build[$key] = array(
          '#type' => 'item',
          '#title' => $definition['label'],
          '#markup' => String::checkPlain(empty($value) ? t('<empty>') : $value),
          '#description' => $element_key . ' (' . $type . ')',
        );
      }
    }
    return $build;
  }

  /**
   * Format config schema as a tree.
   */
  public static function formatDefinitionAsTree($schema, $collapsed = FALSE, $base_key = '') {
    $build = array();
    foreach ($schema as $key => $element) {
      // @todo Clean up array/DataDefinition
      //$definition = $element->getDataDefinition() + array('label' => t('N/A'));
      $definition = $element->getDataDefinition();
      $definition = is_object($definition) ? $definition->toArray() : $definition;
      $definition += array('label' => t('N/A'));

      $type = $definition['type'];
      $element_key = $base_key . $key;
      if ($element instanceof Element) {
        $build[$key] = array(
            '#type' => 'details',
            '#title' => $definition['label'],
            '#description' => $element_key . ' (' . $type . ')',
            '#collapsible' => TRUE,
            '#collapsed' => $collapsed,
          ) + static::formatSchemaASTree($element, TRUE, $element_key . '.');
      }
      else {
        $value = $element->getString();
        $build[$key] = array(
          '#type' => 'item',
          '#title' => $definition['label'],
          '#markup' => String::checkPlain(empty($value) ? t('<empty>') : $value),
          '#description' => $element_key . ' (' . $type . ')',
        );
      }
    }
    return $build;
  }


  /**
   * Format schema data as raw data
   */
  public static function formatSchemaAsRawData($data) {
    return array(
      '#prefix' => '<div class="config-inspector-raw-data"><pre>',
      '#markup' => String::checkPlain((Yaml::encode($data))),
      '#suffix' => '</pre></div>',
    );
  }

  /**
   * Format array as nested tables.
   */
  public static function formatArrayAsNestedTable($data, $title = '', $sanitize_names = TRUE) {
    $rows = array();
    foreach ($data as $name => $value) {
      if (is_array($value)) {
        $element = array(
          '#type' => 'details',
          '#title' => t('show'),
          '#collapsible' => TRUE, '#collapsed' => TRUE,
        );
        $element['table'] = static::formatArrayAsNestedTable($value);
        $content = drupal_render($element);
      }
      else {
        $content = String::checkPlain($value);
      }
      $rows[] = array(
        $sanitize_names ? String::checkPlain($name) : $name,
        $content
      );
    }
    return array(
      '#type' => 'table',
      '#title' => $title,
      '#rows' => $rows,
    );
  }
}
