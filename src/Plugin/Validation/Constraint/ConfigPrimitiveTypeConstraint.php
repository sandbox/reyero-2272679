<?php

/**
 * @file
 * Contains \Drupal\Core\Validation\Plugin\Validation\Constraint\PrimitiveTypeConstraint.
 */

namespace Drupal\config_schema\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Supports validating all primitive types.
 *
 * Example constraint
 *
 * @Plugin(
 *   id = "ConfigPrimitiveType",
 *   label = @Translation("Config Primitive type", context = "Validation")
 * )
 */
class ConfigPrimitiveTypeConstraint extends Constraint {

  public $message = 'This value should be of the correct primitive type.';
}
