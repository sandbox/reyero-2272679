<?php

/**
 * @file
 * Contains \Drupal\Core\Validation\Plugin\Validation\Constraint\ComplexDataConstraintValidator.
 */

namespace Drupal\config_schema\Plugin\Validation\Constraint;

use Drupal\Core\TypedData\ComplexDataInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * Validates complex data.
 *
 * Based on ComplexDataConstraintValidator
 */
class ArrayElementDataValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint) {
    if (!isset($value)) {
      return;
    }
    $typed_data = $this->context->getMetadata()->getTypedData();

    // Check we have schema data for each element.
    if (!is_array($value)) {
      $this->context->addViolation('Value is not an array for %class, value = %value', array(
        '%class' => get_class($typed_data),
        '%value' => is_object($value) ? get_class($value) : (is_array($value) ? 'Array' : (string) $value)
      ));
    }
    else {
      // Check keys have definitions.
      foreach ($value as $key => $data) {
        if (!isset($typed_data[$key])) {
          $this->context->addViolation('Key %key has no schema %class, value = %value', array(
            '%class' => get_class($typed_data),
            '%key' => $key,
            '%value' => is_object($data) ? get_class($data) : (is_array($data) ? 'Array' : (string) $data)
          ));
        }
      }
    }
  }
}
