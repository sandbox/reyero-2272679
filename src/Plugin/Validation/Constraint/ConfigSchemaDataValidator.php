<?php

/**
 * @file
 * Contains \Drupal\config_schema\Plugin\Validation\Constraint\ConfigSchemaDataValidator.
 */

namespace Drupal\config_schema\Plugin\Validation\Constraint;

use Drupal\Core\Validation\Plugin\Validation\Constraint\PrimitiveTypeConstraintValidator;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the ConfigSchemaData constraint.
 */
class ConfigSchemaDataValidator extends ConstraintValidator {

  /**
   * Implements \Symfony\Component\Validator\ConstraintValidatorInterface::validate().
   */
  public function validate($value, Constraint $constraint) {
    $typed_data = $this->context->getMetadata()->getTypedData();

    if (!isset($value)) {
      return;
    }

    // For testing only
    $valid = FALSE;

    // @todo Add validation/s here for generic Element types.

    if (!$valid) {
      // @todo: Provide a good violation message for each problem.
      $this->context->addViolation($constraint->message, array(
        '%class' => get_class($typed_data),
        '%value' => is_object($value) ? get_class($value) : (is_array($value) ? 'Array' : (string) $value)
      ));
    }
  }
}
