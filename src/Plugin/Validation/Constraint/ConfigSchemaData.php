<?php

/**
 * @file
 * Contains \Drupal\config_schema\Plugin\Validation\Constraint\ConfigSchemaDataType.
 */

namespace Drupal\config_schema\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Drupal\Core\Validation\Plugin\Validation\Constraint\PrimitiveTypeConstraint;

/**
 * Length constraint.
 *
 * Overrides the symfony constraint to use Drupal-style replacement patterns.
 *
 * @todo: Move this below the TypedData core component.
 *
 * @Plugin(
 *   id = "ConfigSchemaData",
 *   label = @Translation("Config schema data", context = "Validation"),
 *   type = { "string" }
 * )
 */
class ConfigSchemaData extends Constraint {

  public $message = 'Wrong value for %class: %value';

}
