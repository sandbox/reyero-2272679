<?php

/**
 * @file
 * Contains \Drupal\config_schema\Controller\ConfigSchemaController.
 */

namespace Drupal\config_schema\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Config\StorageInterface;
use Drupal\config_schema\configSchemaManager;
use Drupal\config_schema\Utility\ConfigSchemaFormatter;
use Drupal\config_schema\Config\SchemaDefinition;
use Drupal\Core\Config\Schema\Element;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\config_schema\Config\ExtendedTypedConfigManager;

/**
 * Defines a controller for the config_schema module.
 */
class ConfigSchemaController extends ControllerBase {

  const BASE_PATH = 'admin/config/development/configuration/schema';

  /**
   * @var \Drupal\Core\Config\StorageInterface
   */
  protected $configStorage;

  /**
   * The configuration schema manager.
   *
   * @var \Drupal\config_schema\configSchemaManager
   */
  protected $configSchemaManager;

  /**
   * The configuration schema definition
   *
   */
  protected $configSchemaDefinition;
  /**
   * {@inheritdoc}
   */
  public function __construct(StorageInterface $storage, configSchemaManager $config_schema_manager) {
    $this->storage = $storage;
    $this->configSchemaManager = $config_schema_manager;
    $this->configSchemaDefinition = new SchemaDefinition($this->getTypedConfigManager());
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.storage'),
      $container->get('plugin.manager.config_schema')
    );
  }

  /**
   * Builds a page listing all configuration keys to inspect.
   *
   * @return array
   *   A render array representing the list.
   */
  public function overview() {
    $page['#title'] = $this->t('Schema');
    $page['#attached']['library'][] = 'system/drupal.system.modules';

    $page['filters'] = array(
      '#type' => 'container',
      '#attributes' => array(
        'class' => array('table-filter', 'js-show'),
      ),
    );

    $page['filters']['text'] = array(
      '#type' => 'search',
      '#title' => $this->t('Search'),
      '#size' => 30,
      '#placeholder' => $this->t('Search for a configuration schema'),
      '#attributes' => array(
        'class' => array('table-filter-text'),
        'data-table' => '.config-schema-list',
        'autocomplete' => 'off',
        'title' => $this->t('Enter a part of the configuration key to filter by.'),
      ),
    );

    $page['table'] = array(
      '#type' => 'table',
      '#header' => array(
        //'name' => t('Configuration key'),
        'type' => t('Type'),
        'label' => t('Label'),
        'parent' => t('Parent'),
        //'list' => t('List'),
        //'tree' => t('Tree'),
        //'form' => t('Form'),
        //'validate' => t('Test and validate'),
      ),
      '#attributes' => array(
        'class' => array(
          'config-schema-list',
        ),
      ),
    );
    $type_definitions = $this->configSchemaManager->getAllDefinitions();

    foreach ($type_definitions as $name => $definition) {
      $name = l($name, $this::BASE_PATH . '/' . $name . '/list');
      $type_name = '<span class="table-filter-text-source">' . $name . '</span>';
      $label = $definition->getLabel();
      $parent_name = ($parent = $definition->getParent()) ? $parent->getDataType() : '';
      $page['table'][] = array(
        'type' => array('#markup' => $type_name),
        'label' => array('#markup' => $label ? $label : ''),
        'parent' => array('#markup' => $parent_name ? l($parent_name, $this::BASE_PATH . '/' . $parent_name . '/list') : ''),

      );
    }

    return $page;
  }

  /**
   * List (table) inspection view of the configuration
   *
   * @param $name
   *
   * @return array
   */
  public function getList($name) {
    $page['#title'] = $this->t('Configuration schema type %name', array('%name' => $name));

    $type = $this->getTypedConfigManager()->getSchemaType($name);

    // First get all definitions so tree is built
    $this->configSchemaManager->getAllDefinitions();
    $definition = $this->getTypedConfigManager()->getTypeDefinition($type);

    $page['type'] = $this->renderTypeDefinitions($definition->toArray(), t('Type'));
    // Show parent
    if ($parent = $definition->getParent()) {
      $page['parent'] = $this->renderTypeDefinitions($parent->toArray(), t('Parent type'), TRUE);
    }
    // Show children
    if ($children = $definition->getChildren()) {
      $page['children'] = array(
        '#type' => 'details',
        '#title' => t('Child types'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      );
      $page['children']['table'] = array(
        '#type' => 'table',
        '#header' => array('type' => t('Type'), 'label' => t('Label')),
      );
      foreach ($children as $child) {
        $page['children']['table'][] = $this->renderTypeLabel($child);
      }
    }
    return $page;

  }

  /**
   * Render schema type.
   */
  protected function renderTypeDefinitions($array, $title, $collapsed = FALSE) {
    $build = array(
      '#type' => 'details',
      '#title' => $title,
      // This seems to be buggy, all show collapsed whatever the params:
      '#collapsible' => TRUE, //$collapsed,
      '#collapsed' => $collapsed,
    );
    $build['definition'] = ConfigSchemaFormatter::formatArrayAsNestedTable($array, '', FALSE);
    //$build['definition'] = ConfigSchemaFormatter::formatSchemaAsRawData($array);
    return $build;
  }

  /**
   * Render schema type link
   */
  protected function renderTypeLink($definition, $path = 'list') {
    $type = $definition->getDataType();
    //return l($type, $this::BASE_PATH . '/' . $type . '/' . $path);
    return array(
      '#type' => 'link',
      '#title' => $type,
      '#href' => $this::BASE_PATH . '/' . $type . '/' . $path,
    );
  }

  /**
   * Render schema type and label.
   */
  protected function renderTypeLabel($definition, $path = 'list') {
    return array(
      'type' => $this->renderTypeLink($definition, $path),
      'label' => array('#markup' => ($label = $definition->getLabel()) ? $label : ''),
    );
  }

  /**
   * Tree inspection view of the configuration.
   *
   * @param $name
   *
   * @return array
   */
  public function getTree($name) {
    $page['#title'] = $this->t('Schema tree for %name', array('%name' => $name));
    $type = $this->getTypedConfigManager()->getSchemaType($name);
    // First get all definitions so tree is built
    $this->configSchemaManager->getAllDefinitions();
    $definition = $this->getTypedConfigManager()->getTypeDefinition($type);
    // Calculate main hierarchy to display it uncollapsed.
    $root = $definition->getRoot();
    $parents = array($type => $type);
    while ($definition = $definition->getParent()) {
      $parents[$definition->getDataType()] = $definition->getDataType();
    }
    //$page['tree'] = $this->renderTypeTree($root, $parents);
    /*
    $page['tree'] = array(
      '#type' => 'table',
      '#header' => array(
        'type' => t('Type'),
        'label' => t('Label'),
      ),
    );
    */
    $page['tree'] = $this->renderTableTree($root, $parents);
    return $page;
  }

  /**
   * Render schema type as tree.
   */
  protected function renderTypeTree($definition, $show_types, $depth = 0) {
    $type = $definition->getDataType();
    $build = array(
      '#type' => 'details',
      '#title' => $type,
      '#collapsible' => TRUE,
      '#collapsed' => !in_array($type, $show_types),
    );
    if ($children = $definition->getChildren()) {
      foreach ($children as $child) {
        $build[$child->getDataType()] = $this->renderTypeTree($child, $show_types);
      }
    }

    $build['definition'] = ConfigSchemaFormatter::formatArrayAsNestedTable($definition->toArray());

    return $build;
  }

  /**
   * Render as table tree
   */
  protected function renderTableTree($definition, $show_types, $depth = 0, $maxdepth = NULL) {
    if ($depth == 0) {
      $rows = array(
        '#type' => 'table',
        '#header' => array(
          'type' => t('Type'),
          'label' => t('Label'),
        ),
      );
    }
    else {
      $rows = array();
    }

    $type = $definition->getDataType();
    $type_link = $this->renderTypeLink($definition, 'tree') + array('#prefix' => '', '#suffix' => '');
    if ($depth) {
      $indentation = array('#theme' => 'indentation', '#size' => $depth);
      $type_link['#prefix'] .= drupal_render($indentation);
    }
    if (in_array($type, $show_types)) {
      unset($show_types[$type]);
      $type_link['#prefix'] .= '<strong>';
      $type_link['#suffix'] .= '</strong>';
    }
    $label = $definition->getLabel();
    $rows[] = array(
      'type' => $type_link,
      'label' => array('#markup' => $label ? $label : ''),
    );
    if (!isset($maxdepth) || $depth < $maxdepth) {
      if ($children = $definition->getChildren()) {
        foreach ($children as $child) {
          if (!$show_types || in_array($child->getDataType(), $show_types)) {
            $more = $this->renderTableTree($child, $show_types, $depth + 1);
            $rows = array_merge($rows, $more);
          }
        }
      }
    }
    return $rows;
  }

  /**
   * Validate all configuration data.
   */
  public function validateReport() {
    $page['#title'] = t('Validate configuration data');

    $page['table'] = array(
      '#type' => 'table',
      '#header' => array(
        'name' => t('Configuration key'),
        'result' => t('Validate'),
      ),
      '#attributes' => array(
        'class' => array(
          'config-inspector-list',
        ),
      ),
    );
    foreach ($this->storage->listAll() as $name) {
      if ($data = $this->configSchemaManager->getConfigData($name)) {
        $error_list = $this->configSchemaManager->validateConfigData($name, $data);
        if (count($error_list)) {
          $items = array();
          foreach ($error_list as $violation) {
            $message = t('Error for %key: !message', array(
              '%key' => $violation->getPropertyPath(),
              '!message' => $violation->getMessage(),
            ));
            $items[] = array('#markup' => $message);
          }
          $result = array(
            '#theme' => 'item_list',
            '#items' => $items
          );
        }
        else {
          $result = array('#markup' => $this->t('OK'));
        }
      }
      else {
        $result = array('#markup' => $this->t('No data'));
      }

      $page['table'][] = array(
        'name' => array('#markup' => l($name, $this::BASE_PATH . '/' . $name . '/validate')),
        'result' => $result,
      );
    }

    return $page;
  }


  /**
   * Form based configuration data inspection.
   */
  public function getForm($name) {
    $config_schema = $this->configSchemaManager->getConfigSchema($name);
    $output = \Drupal::formBuilder()->getForm('\Drupal\config_schema\Form\ConfigSchemaItemForm', $config_schema);
    $output['#title'] = $this->t('Raw configuration data for %name', array('%name' => $name));
    return $output;
  }

  /**
   * Raw configuration data inspection.
   */
  public function validateForm($name) {
    $output = \Drupal::formBuilder()->getForm('\Drupal\config_schema\Form\ConfigSchemaValidateForm', $name, $this->configSchemaManager);
    $output['#title'] = $this->t('Validate schema for %name', array('%name' => $name));
    return $output;
  }

  /**
   * Get typed configuration manager.
   *
   * @return \Drupal\Core\Config\TypedConfigManagerInterface
   */
  protected function getTypedConfigManager() {
    return $this->configSchemaManager->getTypedConfigManager();
  }

}
