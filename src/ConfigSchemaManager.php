<?php

/**
 * @file
 * Contains \Drupal\config_schema\ConfigSchemaManager.
 */

namespace Drupal\config_schema;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Config\TypedConfigManager;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Config\Schema\Element;
use Drupal\Core\Config\Schema\ArrayElement;

/**
 * Manages plugins for configuration translation mappers.
 */
class ConfigSchemaManager extends DefaultPluginManager {

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\TypedConfigManagerInterface
   */
  protected $typedConfigManager;


  /**
   * Initialize new configuration schema manager.
   *
   * @param ConfigFactory $config_factory
   * @param TypedConfigManager $typed_config_manager
   */
  public function __construct(ConfigFactory $config_factory, TypedConfigManagerInterface $typed_config_manager) {
    $this->configFactory = $config_factory;
    $this->typedConfigManager = $typed_config_manager;
  }

  /**
   * Get typed configuration manager.
   *
   * @return \Drupal\Core\Config\TypedConfigManagerInterface
   */
  public function getTypedConfigManager() {
    return $this->typedConfigManager;
  }

  /**
   * Provides configuration data.
   *
   * @param string $name
   *   A string config key.
   *
   * @return array|null
   */
  public function getConfigData($name) {
    return $this->config($name)->getValue();
  }

  /**
   * Provides configuration schema.
   *
   * @param string $name
   *   A string config key.
   *
   * @return array|null
   */
  public function getConfigSchema($name) {
    $old_state = $this->configFactory->getOverrideState();
    $this->configFactory->setOverrideState(FALSE);

    $config_schema = $this->typedConfigManager->get($name);

    $this->configFactory->setOverrideState($old_state);

    return $config_schema;
  }

  /**
   * Get an override free configuration object.
   */
  public function config($name) {
    $old_state = $this->configFactory->getOverrideState();
    $this->configFactory->setOverrideState(FALSE);

    $config = $this->typedConfigManager->get($name);

    $this->configFactory->setOverrideState($old_state);

    return $config;
  }

  /**
   * Validate data against schema definition.
   *
   * @param array $data
   *   Configuration data
   * @param array $definition
   *   Schema definition
   *
   * @return \Symfony\Component\Validator\ConstraintViolationListInterface
   *   A list of constraint violations. If the list is empty, validation
   *   succeeded.
   */
  public function validateDataDefinition($data, $definition) {
    $element = $this->typedConfigManager->create($definition, $data);
    return $this->typedConfigManager->validateElement($element);
  }

  /**
   * Validate configuration data against schema.
   *
   * @param  string $name
   *   Configuration name.
   *
   * @param array $data
   *   Configuration data
   *
   * @return \Symfony\Component\Validator\ConstraintViolationListInterface
   *   A list of constraint violations. If the list is empty, validation
   *   succeeded.
   */
  public function validateConfigData($name, $data = NULL) {
    return $this->typedConfigManager->validateConfigData($name, $data);
  }

  /**
   * Get all definitions.
   *
   * @return array
   *   Array of TypeDefinition objects.
   */
  public function getAllDefinitions() {
    $types = array_keys($this->typedConfigManager->getDefinitions());
    return array_map(array($this->typedConfigManager, 'getTypeDefinition'), array_combine($types, $types));
  }

  /**
   * Get root type definitions.
   *
   * @return array
   *   Array of TypeDefinition objects.
   */
  public function getBaseDefinitions() {
    return array_filter($this->getAllDefinitions(), array($this, 'isRoot'));
  }

  /**
   * Helper function for filtering.
   */
  public function hasParent(TypeDefinition $schema) {
    return (boolean)$schema->getParent();
  }

  /**
   * Helper function for filtering.
   */
  public function isRoot(TypeDefinition $schema) {
    return !$this->hasParent($schema);
  }

}
