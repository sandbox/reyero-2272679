<?php

/**
 * @file
 * Contains \Drupal\config_schema\Form\ConfigSchemaValidateForm.
 */

namespace Drupal\config_schema\Form;

use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Config\Schema\Element;
use Drupal\config_schema\ConfigSchemaManager;

/**
 * Defines a form for editing configuration translations.
 */
class ConfigSchemaValidateForm extends FormBase {

  /**
   * The configuration schema manager.
   *
   * @var \Drupal\config_schema\ConfigSchemaManager
   */
  protected $configSchemaManager;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'config_schema_raw_data_form';
  }

  /**
   * Build configuration form with metadata and values.
   */
  public function buildForm(array $form, array &$form_state, $name = NULL, ConfigSchemaManager $manager = NULL) {

    $this->configSchemaManager = $manager;
    $typedConfig = $manager->getTypedConfigManager();

    $form['#title'] = $this->t('Raw configuration data for %name', array('%name' => $name));
    $form['#attached']['library'][] = 'config_schema/drupal.config_schema.admin';

    $data = $manager->getConfigData($name);
    $definition = $typedConfig->hasConfigSchema($name) ? $typedConfig->getDefinition($name) : array();

    $form['data'] = array(
      '#title' => 'Configuration data',
      '#type' => 'textarea',
      '#rows' => 20,
      '#required' => TRUE,
      '#default_value' => $data ? Yaml::encode($data) : '',
    );
    $form['schema'] = array(
      '#title' => 'Configuration schema',
      '#type' => 'textarea',
      '#rows' => 20,
      '#value' => $definition ? Yaml::encode($definition) : '',
      '#disabled' => TRUE,
    );

     // Add some information to the form state for easier form altering.
    $form_state['config_name'] = $name;
    $form_state['config_data'] = $data;
    $form_state['config_schema'] = $definition;

    $form['actions']['#type'] = 'actions';

    $form['actions']['validate'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Validate schema'),
      '#submit' => array(array($this, 'validateSchema')),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, array &$form_state) {
    // Decode the submitted data.
    try {
      $data = Yaml::decode($form_state['values']['data']);
    }
    catch (\Exception $e) {
      $this->setFormError('data', $form_state, $e->getMessage());
    }
    if (empty($data)) {
      $this->setFormError('data', $form_state, $this->t('Empty configuration data.'));
      return;
    }

    // Store the decoded version of the submitted import.
    form_set_value($form['data'], $data, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, array &$form_state) {
    $name = $form_state['config_name'];
    $definition = $form_state['config_schema'];
    $data = $form_state['values']['data'];

    // Check we do have a definition.
    if (!$definition) {
      $this->setFormError('data', $form_state, $this->t('Empty schema.'));
      return;
    }

    // We should get a ConstraintViolationListInterface
    $list = $this->configSchemaManager->validateConfigData($name, $data);
    if (count($list)) {
      foreach ($list as $index => $violation) {
        drupal_set_message($this->t('Schema validation error for %key: !message', array(
          '%key' => $violation->getPropertyPath(),
          '!message' => $violation->getMessage(),
        )), 'warning');
      }
    }
    else {
      drupal_set_message($this->t('Passed basic schema validation.'));
    }
  }

}
