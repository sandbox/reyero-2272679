<?php

/**
 * @file
 * Contains \Drupal\config_schema\Form\ConfigSchemaItemForm.
 */

namespace Drupal\config_schema\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Config\Schema\Element;

/**
 * Defines a form for editing configuration data.
 */
class ConfigSchemaItemForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'config_schema_item_form';
  }

  /**
   * Build configuration form with metadata and values.
   */
  public function buildForm(array $form, array &$form_state, $schema = NULL) {
    $form['structure'] = $this->buildFormConfigElement($schema);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, array &$form_state) {

  }

  /**
   * Format config schema as a tree.
   */
  protected function buildFormConfigElement($schema, $collapsed = FALSE) {
    $build = array();
    foreach ($schema as $key => $element) {
      // @todo Clean up array/DataDefinition
      //$definition = $element->getDataDefinition() + array('label' => t('N/A'));
      $definition = $element->getDataDefinition();
      $definition = is_object($definition) ? $definition->toArray() : $definition;
      $definition += array('label' => t('N/A'));

      if ($element instanceof Element) {
        $build[$key] = array(
            '#type' => 'details',
            '#title' => $definition['label'],
            '#collapsible' => TRUE,
            '#collapsed' => $collapsed,
          ) + $this->buildFormConfigElement($element, TRUE);
      }
      else {
        $type = $definition['type'];
        switch ($type) {
          case 'boolean':
            $type = 'checkbox';
            break;
          case 'string':
          case 'color_hex':
          case 'path':
          case 'label':
            $type = 'textfield';
            break;
          case 'text':
            $type = 'textarea';
            break;
          case 'integer':
            $type = 'number';
            break;
        }
        $value = $element->getString();
        $build[$key] = array(
          '#type' => $type,
          '#title' => $definition['label'],
          '#default_value' => $value,
        );
      }
    }
    return $build;
  }

}
