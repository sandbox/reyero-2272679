<?php

/**
 * @file
 * Contains \Drupal\config_schema\Config\TypeDefinition.
 */

namespace Drupal\config_schema\Config;

use Drupal\Core\TypedData\DataDefinition;
use Drupal\Component\Utility\NestedArray;

/**
 * A schema definition class for defining data based on defined data types.
 */
class TypeDefinition extends DataDefinition {

  /**
   * Parent schema definition
   *
   * @var SchemaDefinition.
   */
  protected $parent;

  /**
   * Children schema definitions
   *
   * @var array
   */
  protected $children;

  /**
   * Set parent definition and add this one as a child.
   */
  public function setParent(TypeDefinition $parent) {
    $this->parent = $parent;
    if ($parent) {
      $this->definition = NestedArray::mergeDeep($parent->toArray(), $this->definition);
      $parent->addChild($this);
    }
  }

  /**
   * Get children types (derived).
   *
   * @return array
   */
  public function getChildren() {
    return isset($this->children) ? $this->children : array();
  }

  /**
   * Get parent type.
   *
   * @return TypeDefinition|FALSE
   */
  public function getParent() {
    return isset($this->parent) ? $this->parent : NULL;
  }

  /**
   * Get the root of the definition tree.
   */
  public function getRoot() {
    if (isset($this->parent)) {
      return $this->parent->getRoot();
    }
    // If no parent is set, this is the root of the data tree.
    return $this;
  }

  /**
   * Add child type to list.
   */
  public function addChild(TypeDefinition $schema) {
    $this->children[$schema->getDataType()] = $schema;
  }

  /**
   * Render as tree
   */
  public function renderTree($collapsed = FALSE) {
    if ($children = $this->getChildren()) {
      $build = array(
        '#type' => 'details',
        '#title' => $this->getDataType(),
        '#collapsible' => TRUE,
        '#collapsed' => $collapsed,
      );
      foreach ($children as $type => $child) {
        $build[$type] = $child->renderTree($collapsed);
      }
    }
    else {
      $build = array(
        '#markup' => $this->getDataType(),
      );
    }
    return $build;
  }

  /**
   * Convert to nested array.
   */
  public function toNestedArray() {
    $array = $this->definition;
    foreach ($this->getChildren() as $type => $child) {
      $array[$type] = $child->toNestedArray();
    }
    return $array;
  }
}