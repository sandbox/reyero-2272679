<?php

/**
 * @file
 * Contains \Drupal\config_schema\Config\ExtendedTypedConfigManager.
 */

namespace Drupal\config_schema\Config;

use Drupal\Core\Config\TypedConfigManager;
use Drupal\Core\Config\StorageInterface;
use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Component\Plugin\PluginManagerBase;
use Drupal\Component\Utility\NestedArray;
use Drupal\Component\Utility\String;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\DataDefinitionInterface;
use Drupal\Core\TypedData\Validation\MetadataFactory;
use Drupal\Core\Validation\ConstraintManager;
use Drupal\Core\Validation\DrupalTranslator;
use Symfony\Component\Validator\ValidatorInterface;
use Symfony\Component\Validator\Validation;

use Drupal\config_schema\Validation\ConfigSchemaMetadataFactory;


/**
 * Extended manager for config type plugins, adding:
 *
 * - Schema validation
 */
class ExtendedTypedConfigManager extends TypedConfigManager {

  /**
   * The validator used for validating typed data.
   *
   * @var \Symfony\Component\Validator\ValidatorInterface
   */
  protected $validator;

  /**
   * The validation constraint manager to use for instantiating constraints.
   *
   * @var \Drupal\Core\Validation\ConstraintManager
   */
  protected $constraintManager;

  /**
   * Schema definitions.
   *
   * @var array
   *   Array of TypeDefinition objects
   */
  protected $type_definitions = array();

  /**
   * Get schema type (definition type name).
   */
  public function getSchemaType($base_plugin_id) {
    $definitions = $this->getDefinitions();
    if (isset($definitions[$base_plugin_id])) {
      $type = $base_plugin_id;
    }
    elseif (strpos($base_plugin_id, '.') && $name = $this->getFallbackName($base_plugin_id)) {
      // Found a generic name, replacing the last element by '*'.
      $type = $name;
    }
    else {
      // If we don't have definition, return the 'undefined' element.
      $type = 'undefined';
    }
    return $type;
  }

  /**
   * Sets the validator for validating typed data.
   *
   * @param \Symfony\Component\Validator\ValidatorInterface $validator
   *   The validator object to set.
   */
  public function setValidator(ValidatorInterface $validator) {
    $this->validator = $validator;
  }

  /**
   * Gets the validator for validating typed data.
   *
   * @return \Symfony\Component\Validator\ValidatorInterface
   *   The validator object.
   */
  public function getValidator() {
    if (!isset($this->validator)) {
      $this->validator = Validation::createValidatorBuilder()
        ->setMetadataFactory(new ConfigSchemaMetadataFactory($this))
        ->setTranslator(new DrupalTranslator())
        ->getValidator();
    }
    return $this->validator;
  }

  /**
   * Sets the validation constraint manager.
   *
   * The validation constraint manager is used to instantiate validation
   * constraint plugins.
   *
   * @param \Drupal\Core\Validation\ConstraintManager
   *   The constraint manager to set.
   */
  public function setValidationConstraintManager(ConstraintManager $constraintManager) {
    $this->constraintManager = $constraintManager;
  }

  /**
   * Gets the validation constraint manager.
   *
   * @return \Drupal\Core\Validation\ConstraintManager
   *   The constraint manager.
   */
  public function getValidationConstraintManager() {
    return $this->constraintManager;
  }

  /**
   * Validate typed configuration element
   *
   * @return \Symfony\Component\Validator\ConstraintViolationListInterface
   *   A list of constraint violations. If the list is empty, validation
   *   succeeded.
   */
  public function validateElement($element) {
    $validator = $this->getValidator();
    return $validator->validate($element);
  }

  /**
   * Validate configuration data against definition.
   *
   * @return \Symfony\Component\Validator\ConstraintViolationListInterface
   *   A list of constraint violations. If the list is empty, validation
   *   succeeded.
   */
  public function validateConfigData($name, $data = NULL) {
    if (!isset($data)) {
      $element = $this->get($name);
    }
    else {
      $definition = $this->getDefinition($name);
      $element = $this->create($definition, $data);
    }
    return $this->validateElement($element);
  }

  /**
   * Gets configured constraints from a data definition.
   *
   * @see \Drupal\Core\Validation\ConstraintManager
   * @see \Druapl\Core\TypedData\TypedDataManager:getConstraints()
   *
   * @param array | \Drupal\Core\TypedData\DataDefinitionInterface $definition
   *   A data definition.
   *
   * @return array
   *   Array of constraints, each being an instance of
   *   \Symfony\Component\Validator\Constraint.
   *
   * @todo: Having this as well as $definition->getConstraints() is confusing.
   *
   * @todo: Definition should be DataDefinitionInterface
   */
  //public function getConstraints(DataDefinitionInterface $definition) {
  public function getConstraints($definition) {

    $constraints = array();

    $validation_manager = $this->getValidationConstraintManager();

    //$type_definition = $this->getDefinition($definition->getDataType());
    $type_definition = is_object($definition) ? $definition->toArray() : $definition;

    // Auto-generate a constraint for data types implementing a primitive
    // interface.
    if (is_subclass_of($type_definition['class'], '\Drupal\Core\TypedData\PrimitiveInterface')) {
      $constraints[] = $validation_manager->create('PrimitiveType', array());
    }
    // Auto-generate constraint for Schema Element data types
    if (is_subclass_of($type_definition['class'], '\Drupal\Core\Config\Schema\ArrayElement')) {
      $constraints[] = $validation_manager->create('ArrayElementData', array());
    }
    elseif (is_subclass_of($type_definition['class'], '\Drupal\Core\Config\Schema\Element')) {
      $constraints[] = $validation_manager->create('ConfigSchemaData', array());
    }

    // Add in constraints specified by the data type.
    if (isset($type_definition['constraints'])) {
      foreach ($type_definition['constraints'] as $name => $options) {
        $constraints[] = $validation_manager->create($name, $options);
      }
    }

    // Add any constraints specified as part of the data definition.
    // @todo Uncomment if we ever get to proper DataDefinition objects
    if (is_object($definition)) {
      $defined_constraints = $definition->getConstraints();
      foreach ($defined_constraints as $name => $options) {
        $constraints[] = $validation_manager->create($name, $options);
      }
      // Add the NotNull constraint for required data.
      if ($definition->isRequired() && !isset($defined_constraints['NotNull'])) {
        $constraints[] = $validation_manager->create('NotNull', array());
      }


      // If the definition does not provide a class use the class from the type
      // definition for performing interface checks.
      $class = $definition->getClass();
      if (!$class) {
        $class = $type_definition['class'];
      }
      // Check if the class provides allowed values.
      if (is_subclass_of($class,'Drupal\Core\TypedData\AllowedValuesInterface')) {
        $constraints[] = $validation_manager->create('AllowedValues', array());
      }
    }

    return $constraints;
  }

  /**
   * Gets type definition.
   *
   * @return TypeDefinition|NULL
   */
  public function getTypeDefinition($type) {
    if (!array_key_exists($type, $this->type_definitions)) {
      if (isset($this->definitions[$type])) {
        $definition = $this->definitions[$type];
        // Resolve parent dependencies and set type.
        if (isset($definition['type'])) {
          $parent = $this->getTypeDefinition($definition['type']);
        }
        $definition['type'] = $type;
        $schema = new TypeDefinition($definition);
        if (isset($parent)) {
          $schema->setParent($parent);
        }
        $this->type_definitions[$type] = $schema;
      }
      else {
        $this->type_definitions[$type] = NULL;
      }
    }
    return $this->type_definitions[$type];
  }
}
