<?php

/**
 * @file
 * Contains \Drupal\config_schema\Config\SchemaDefinition.
 */

namespace Drupal\config_schema\Config;

use Drupal\Core\Config\TypedConfigManagerInterface;

/**
 * A schema definition class for defining data based on defined data types.
 */
class SchemaDefinition {

  /**
   * Existing array definitions.
   *
   * @var array
   *   Array of definition arrays
   */
  protected $definitions;

  /**
   * Schema definitions.
   *
   * @var array
   *   Array of TypeDefinition objects
   */
  protected $schema = array();

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\TypedConfigManagerInterface
   */
  protected $typedConfigManager;

  /**
   * Constructs a new definition tree.
   */
  public function __construct(TypedConfigManagerInterface $manager) {
    $this->typedConfigManager = $manager;
    $this->definitions = $manager->getDefinitions();
  }

  /**
   * Gets type definition.
   *
   * @return TypeDefinition|NULL
   */
  public function getTypeDefinition($type) {
    if (!array_key_exists($type, $this->schema)) {
      if (isset($this->definitions[$type])) {
        $definition = $this->definitions[$type];
        // Resolve parent dependencies and set type.
        if (isset($definition['type'])) {
          $parent = $this->getTypeDefinition($definition['type']);
        }
        $definition['type'] = $type;
        $schema = new TypeDefinition($definition);
        if (isset($parent)) {
          $schema->setParent($parent);
        }
        $this->schema[$type] = $schema;
      }
      else {
        $this->schema[$type] = NULL;
      }
    }
    return $this->schema[$type];
  }

  /**
   * Get all definitions.
   *
   * @return array
   *   Array of TypeDefinition objects.
   */
  public function getAllDefinitions() {
    return array_map(array($this, 'getTypeDefinition'), array_keys($this->definitions));
  }

  /**
   * Get root type definitions.
   *
   * @return array
   *   Array of TypeDefinition objects.
   */
  public function getBaseDefinitions() {
    return array_filter($this->getAllDefinitions(), array($this, 'isRoot'));
  }

  /**
   * Helper function for filtering.
   */
  public function hasParent(TypeDefinition $schema) {
    return (boolean)$schema->getParent();
  }

  /**
   * Helper function for filtering.
   */
  public function isRoot(TypeDefinition $schema) {
    return !$this->hasParent($schema);
  }

}