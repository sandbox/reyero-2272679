<?php

/**
 * @file
 * Contains \Drupal\config_schema\Validation\ArrayElementMetadata.
 */

namespace Drupal\config_schema\Validation;

use Drupal\Core\TypedData\ComplexDataInterface;
use Drupal\Core\TypedData\ListInterface;
use Symfony\Component\Validator\PropertyMetadataContainerInterface;
use Symfony\Component\Validator\ValidationVisitorInterface;
use Symfony\Component\Validator\MetadataFactoryInterface;

use Drupal\Core\Config\Schema\Element;
use Drupal\Core\Config\Schema\ArrayElement;

/**
 * Typed data implementation of the validator MetadataInterface.
 */
class ArrayElementMetadata extends ElementMetadata implements PropertyMetadataContainerInterface {

  /**
   * Overrides Metadata::accept().
   *
   * @param ArrayElement $typed_data
   */
  public function accept(ValidationVisitorInterface $visitor, $typed_data, $group, $propertyPath) {
    // To let all constraints properly handle empty structures, pass on NULL
    // if the data structure is empty. That way existing NotNull or NotBlank
    // constraints work as expected.
    if ($typed_data->isEmpty()) {
      $value = NULL;
    }
    else {
      $value = $typed_data->getValue();
    }
    $visitor->visit($this, $value , $group, $propertyPath);

    $pathPrefix = isset($propertyPath) && $propertyPath !== '' ? $propertyPath . '.' : '';

    if ($typed_data) {
      // Yes, we can iterate over ArrayElement
      foreach ($typed_data as $name => $data) {
        $metadata = $this->factory->getMetadataFor($data, $name);
        $metadata->accept($visitor, $data, $group, $pathPrefix . $name);
      }
    }
  }

  /**
   * Implements PropertyMetadataContainerInterface::hasPropertyMetadata().
   */
  public function hasPropertyMetadata($property_name) {
    try {
      $exists = (bool)$this->getPropertyMetadata($property_name);
    }
    catch (\LogicException $e) {
      $exists = FALSE;
    }
    return $exists;
  }

  /**
   * Implements PropertyMetadataContainerInterface::getPropertyMetadata().
   */
  public function getPropertyMetadata($property_name) {
    if ($this->typedData instanceof ListInterface) {
      return array(new Metadata($this->typedData[$property_name], $property_name));
    }
    elseif ($this->typedData instanceof ComplexDataInterface) {
      return array(new Metadata($this->typedData->get($property_name), $property_name));
    }
    else {
      throw new \LogicException("There are no known properties.");
    }
  }
}
