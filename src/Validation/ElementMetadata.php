<?php

/**
 * @file
 * Contains \Drupal\config_schema\Validation\ElementMetadata.
 */

namespace Drupal\config_schema\Validation;

use Drupal\Core\TypedData\TypedDataInterface;
use Symfony\Component\Validator\ValidationVisitorInterface;
use Symfony\Component\Validator\PropertyMetadataInterface;
use Symfony\Component\Validator\MetadataFactoryInterface;
use Drupal\Core\TypedData\Validation\Metadata;
use Drupal\Core\Config\TypedConfigManagerInterface;

use Drupal\Core\Config\Schema\Element;
use Drupal\Core\Config\Schema\ArrayElement;

/**
 * Typed data implementation of the validator MetadataInterface.
 */
class ElementMetadata extends Metadata {

  /**
   * @var Drupal\Core\Config\TypedConfigManagerInterface
   */
  protected $typedConfigManager;

  /**
   * Constructs the object.
   *
   * @param \Drupal\Core\TypedData\TypedDataInterface $typed_data
   *   The typed data object the metadata is about.
   * @param $name
   *   The name of the property to get metadata for. Leave empty, if
   *   the data is the root of the typed data tree.
   * @param \Drupal\Core\TypedData\Validation\MetadataFactory $factory
   *   The factory to use for instantiating property metadata.
   */
  public function __construct(TypedDataInterface $typed_data, $name = '', MetadataFactoryInterface $factory, TypedConfigManagerInterface $typedConfigManager) {
    parent::__construct($typed_data, $name, $factory);
    $this->typedConfigManager = $typedConfigManager;
  }

  /**
   * Implements MetadataInterface::accept().
   */
  public function accept(ValidationVisitorInterface $visitor, $typed_data, $group, $propertyPath) {

    // @todo: Do we have to care about groups? Symfony class metadata has
    // $propagatedGroup.

    $visitor->visit($this, $typed_data->getValue(), $group, $propertyPath);
  }

  /**
   * Implements MetadataInterface::findConstraints().
   */
  public function findConstraints($group) {
    //return $this->typedData->getConstraints();
    return $this->typedConfigManager->getConstraints($this->typedData->getDataDefinition(), $group);
  }

}
