<?php

/**
 * @file
 * Contains \Drupal\config_schema\Validation\ConfigSchemaMetadataFactory.
 */

namespace Drupal\config_schema\Validation;
//namespace Drupal\Core\TypedData\Validation;

use Drupal\Core\TypedData\ComplexDataInterface;
use Drupal\Core\TypedData\ListInterface;
use Drupal\Core\TypedData\TypedDataInterface;
use Symfony\Component\Validator\MetadataFactoryInterface;
use Drupal\Core\TypedData\Validation\MetadataFactory;
use Drupal\Core\Config\TypedConfigManagerInterface;

use Drupal\Core\Config\Schema\Element;
use Drupal\Core\Config\Schema\ArrayElement;

/**
 * Typed data implementation of the validator MetadataFactoryInterface.
 */
class ConfigSchemaMetadataFactory extends MetadataFactory {

  /**
   * @var Drupal\Core\Config\TypedConfigManagerInterface
   */
  protected $typedConfigManager;

  /**
   * @param Drupal\Core\Config\TypedConfigManagerInterface $typedConfigManager
   */
  public function __construct($typedConfigManager) {
    $this->typedConfigManager = $typedConfigManager;
  }

  /**
   * Implements MetadataFactoryInterface::getMetadataFor().
   *
   * @param \Drupal\Core\TypedData\TypedDataInterface $typed_data
   *   Some typed data object containing the value to validate.
   * @param $name
   *   (optional) The name of the property to get metadata for. Leave empty, if
   *   the data is the root of the typed data tree.
   */
  public function getMetadataFor($typed_data, $name = '') {
    if ($typed_data instanceof ArrayElement) {
      $class = '\\Drupal\\config_schema\\Validation\\ArrayElementMetadata';
    }
    elseif ($typed_data instanceof Element){
      $class = '\\Drupal\\config_schema\\Validation\\ElementMetadata';
    }
    // Regular TypedData, Maybe better invoke the parent class
    elseif (!$typed_data instanceof TypedDataInterface) {
      throw new \InvalidArgumentException('The passed value must be a typed data object.');
    }
    else {
      $is_container = $typed_data instanceof ComplexDataInterface || $typed_data instanceof ListInterface;
      $class = '\\Drupal\\config_schema\\Validation\\' . ($is_container ? 'ArrayElementMetadata' : 'ElementMetadata');
   }
   return new $class($typed_data, $name, $this, $this->typedConfigManager);
  }

  /**
   * Implements MetadataFactoryInterface::hasMetadataFor().
   *
   * @todo Remove, use the parent class.
   */
  public function hasMetadataFor($value) {
    return $value instanceof TypedDataInterface;
  }
}
